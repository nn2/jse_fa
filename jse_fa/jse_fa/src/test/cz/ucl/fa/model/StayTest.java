package test.cz.ucl.fa.model; 

import cz.ucl.fa.model.Accommodation;
import cz.ucl.fa.model.Stay;
import cz.ucl.fa.model.util.JPAUtil;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;

import java.text.DateFormat;
import java.util.Date;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/** 
* Stay Tester. 
* 
* @author <Authors name> 
* @since <pre>Led 25, 2015</pre> 
* @version 1.0 
*/ 
public class StayTest { 

@Before
public void before() throws Exception { 
} 

@After
public void after() throws Exception { 
} 

/**
* 
* Method: canAccommodate(int numberOfPeople) 
* 
*/ 
@Test
public void testCanAccommodate() throws Exception {
    Accommodation hotel = JPAUtil.getEntityManager().find(Accommodation.class, Long.parseLong("5"));
    Stay stay = new Stay();

    DateFormat df = DateFormat.getDateInstance();

    stay.setHotel(hotel);
    stay.setDayFrom(df.parse("10.2.2009"));
    stay.setDayTo(df.parse("14.2.2009"));

    assertTrue(stay.canAccommodate(5));
    assertFalse(stay.canAccommodate(18));

    // More than hotel capacity (20)
    assertFalse(stay.canAccommodate(21));


}

}
