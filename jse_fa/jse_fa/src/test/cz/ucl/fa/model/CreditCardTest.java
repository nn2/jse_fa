package test.cz.ucl.fa.model; 

import cz.ucl.fa.model.CreditCard;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/** 
* CreditCard Tester. 
* 
* @author <Authors name> 
* @since <pre>Led 25, 2015</pre> 
* @version 1.0 
*/ 
public class CreditCardTest {

    private CreditCard cc;

@Before
public void before() throws Exception {
    cc = new CreditCard();

    cc.setNumber("5555222255552222");
} 

@After
public void after() throws Exception { 
} 

/**
* 
* Method: normalizeNumber(String cardNumber) 
* 
*/ 
@Test
public void testNormalizeNumber() throws Exception {
    CreditCard cc = new CreditCard();

    cc.setNumber("5555-5555-5555-5555");

    assertEquals("5555555555555555", cc.getNumber());
}

/** 
* 
* Method: verifyNumber(String cardNumber) 
* 
*/ 
@Test
public void testVerifyNumber() throws Exception {
    assertTrue(CreditCard.verifyNumber("5555-5555-5555-5555"));
    assertFalse(CreditCard.verifyNumber("6"));
}

/** 
* 
* Method: obfuscate(String cardNumber) 
* 
*/ 
@Test
public void testObfuscate() throws Exception { 
    assertEquals("XXXX-XXXX-XXXX-5555", CreditCard.obfuscate("5555-5555-5555-5555"));
} 

}
