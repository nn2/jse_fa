package cz.ucl.fa.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.text.DateFormat;
import java.util.Date;

/**
 * Created by Ondra on 23. 1. 2015.
 */
@Entity
public class Traveller {

    @Id
    @GeneratedValue
    private long id;
    private Date dateOfBirth;
    private String firstName;
    private String surname;
    private String idNumber;

    @ManyToOne
    private Contract contract;

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    @Override
    public String toString() {
        DateFormat df = DateFormat.getDateInstance();
        return String.format("%s %s (%s)", surname, firstName, df.format(dateOfBirth));
    }
}
