package cz.ucl.fa.model.util;

import org.hibernate.dialect.MySQL5InnoDBDialect;

import java.sql.Types;

/**
 * Created by Ondra on 23. 1. 2015.
 */
public class CustomMySQL5InnoDBDialect extends MySQL5InnoDBDialect {

    public CustomMySQL5InnoDBDialect() {
        super();
        registerColumnType(Types.BOOLEAN, "bit(1)");
    }
}
