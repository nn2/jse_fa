package cz.ucl.fa.model;

import javax.persistence.*;
import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by Ondra on 23. 1. 2015.
 */
@Entity
public class Contract {

    @Id
    @GeneratedValue
    private long id;

    private long number;

    @ManyToOne
    private Customer customer;

    @ManyToOne
    private Holiday holiday;

    @OneToMany(mappedBy = "contract")
    private List<Traveller> travellers;

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Holiday getHoliday() {
        return holiday;
    }

    public void setHoliday(Holiday holiday) {
        this.holiday = holiday;
    }

    public List<Traveller> getTravellers() {
        return travellers;
    }

    public Contract() {
        travellers = new ArrayList<Traveller>();
    }

    public void setTravellers(List<Traveller> travellers) {
        this.travellers = travellers;
    }

    /**
     * Build JTree content for displaying
     *
     * @param rootNode Root node to append data to
     * @param contract Contract to use for building
     */
    public static void buildContractTree(DefaultMutableTreeNode rootNode, Contract contract) {
        DefaultMutableTreeNode contractTreeNode = new DefaultMutableTreeNode("Contract " + contract.getNumber());

        DefaultMutableTreeNode travelNode = new DefaultMutableTreeNode("Travel");

        for (Transportation t : contract.getHoliday().getTransportations()) {
            DefaultMutableTreeNode transportNode = new DefaultMutableTreeNode(t.toString());
            travelNode.add(transportNode);
        }

        DefaultMutableTreeNode travellersNode = new DefaultMutableTreeNode("Travellers");
        for (Traveller t : contract.getTravellers()) {
            DefaultMutableTreeNode travellerNode = new DefaultMutableTreeNode(t.toString());
            travellersNode.add(travellerNode);
        }

        DefaultMutableTreeNode accommodationNode = new DefaultMutableTreeNode("Accommodation");

        for (Stay s : contract.getHoliday().getStays()) {
            DefaultMutableTreeNode stayNode = new DefaultMutableTreeNode(s.toString());
            accommodationNode.add(stayNode);
        }

        DefaultMutableTreeNode servicesNode = new DefaultMutableTreeNode("Services");

        for (Service s : contract.getHoliday().getServices()) {
            DefaultMutableTreeNode serviceNode = new DefaultMutableTreeNode(s.toString());
            servicesNode.add(serviceNode);
        }


        contractTreeNode.add(travelNode);
        contractTreeNode.add(travellersNode);
        contractTreeNode.add(accommodationNode);
        contractTreeNode.add(servicesNode);
        rootNode.add(contractTreeNode);
    }

    /**
     * Export contract content to file. Expects valid contract object!
     */
    public void exportToFile() {
        Properties prop = new Properties();
        String propFilename = "config.properties";
        String outputPath = "./";
        try {
            prop.load(new FileInputStream(propFilename));
            outputPath = prop.getProperty("outputPath","./");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {

            BufferedWriter out = new BufferedWriter(new FileWriter(outputPath + "contract_"+number+".txt"));

            out.write("Contract: " + number);
            out.newLine();

            out.write("Customer: "+getCustomer().getFullName());
            out.newLine();

            out.write("Holiday: "+getHoliday().getName());
            out.newLine();

            out.write("Start: " + getHoliday().getStarts());
            out.newLine();

            out.write("Ends: "+getHoliday().getEnds());
            out.newLine();

            out.write("Basic price: "+getHoliday().getPrice());
            out.newLine();

            out.write("Total price: "+getHoliday().getPrice()*getTravellers().size());
            out.newLine();
            out.write("----------------------------------------");
            out.newLine();

            out.write("Travellers:");
            out.newLine();
            out.write("----------------------------------------");
            out.newLine();

            for (Traveller t : getTravellers()) {
                out.write(t.toString());
                out.newLine();
            }

            out.write("----------------------------------------");
            out.newLine();
            out.write("Transportations:");
            out.newLine();
            out.write("----------------------------------------");
            out.newLine();

            for (Transportation t : getHoliday().getTransportations()) {
                out.write(t.toString());
                out.newLine();
            }

            out.write("----------------------------------------");
            out.newLine();
            out.write("Accommodations:");
            out.newLine();
            out.write("----------------------------------------");
            out.newLine();

            for (Stay s : getHoliday().getStays()) {
                out.write(s.toString());
                out.newLine();
            }

            out.write("----------------------------------------");
            out.newLine();
            out.write("Services:");
            out.write("----------------------------------------");
            out.newLine();
            out.newLine();

            for (Service s : getHoliday().getServices()) {
                out.write(s.toString());
                out.newLine();
            }

            out.close();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "File export error", "File error", JOptionPane.ERROR_MESSAGE);
        }
    }

}
