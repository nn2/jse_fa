package cz.ucl.fa.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Ondra on 23. 1. 2015.
 */
@Entity
public class CreditCard {

    @Id
    @GeneratedValue
    private long id;
    private String number;
    private String ownerName;
    private String validity;

    @OneToMany(mappedBy = "card")
    private List<Customer> customer;

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = normalizeNumber(number);
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    /**
     * Returns normalized card number - removes everything except numbers
     * @param cardNumber
     * @return
     */
    public static String normalizeNumber(String cardNumber) {
        String pattern = "[^0-9]";
        return cardNumber.replaceAll(pattern, "");
    }

    /**
     * Verifies card number for valid visa, mastercard, jcb, diners, amex
     * @param cardNumber
     * @return
     */
    public static boolean verifyNumber(String cardNumber) {
        String pattern = "^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|6(?:011|5[0-9]{2})[0-9]{12}|(?:2131|1800|35\\d{3})\\d{11})$";
        return normalizeNumber(cardNumber).matches(pattern);
    }

    /**
     * Returns last 4 digits of card number, xxxx-ing the rest
     * @param cardNumber
     * @return
     */
    public static String obfuscate(String cardNumber) {
        String pattern = "(.*)([0-9]{4})$";

        return normalizeNumber(cardNumber).replaceAll(pattern, "XXXX-XXXX-XXXX-$2");
    }

    @Override
    public String toString() {
        return String.format("%s, expires %s", obfuscate(number), validity);
    }

    public CreditCard() {

    }
}
