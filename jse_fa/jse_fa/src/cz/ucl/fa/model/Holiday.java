package cz.ucl.fa.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@NamedQueries( {
        @NamedQuery(name = "Holiday.getFixed", query = "SELECT DISTINCT h from Holiday h where h.fixed = true"),
})
public class Holiday {

    @Id
    @GeneratedValue
    private long id;
    private boolean fixed;
    private String name;
    private Double price;
    private Date starts;
    private Date ends;

    @OneToMany
    @JoinColumn(name = "holiday_id")
    private List<Contract> contracts;

    @ManyToMany
    private List<Service> services;

    @OneToMany(mappedBy = "holiday", cascade = CascadeType.ALL)
    private List<Stay> stays;

    @ManyToMany
	@JoinTable(name = "holiday_transportation", joinColumns = { @JoinColumn(name="holiday_id")}, inverseJoinColumns = { @JoinColumn(name = "transportation_id")})
    private List<Transportation> transportations;

    public boolean isFixed() {
        return fixed;
    }

    public void setFixed(boolean fixed) {
        this.fixed = fixed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getStarts() {
        return starts;
    }

    public void setStarts(Date starts) {
        this.starts = starts;
    }

    public Date getEnds() {
        return ends;
    }

    public void setEnds(Date ends) {
        this.ends = ends;
    }

    public List<Contract> getContracts() {
        return contracts;
    }

    public void setContracts(List<Contract> contracts) {
        this.contracts = contracts;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public List<Stay> getStays() {
        return stays;
    }

    public void setStays(List<Stay> stays) {
        this.stays = stays;
    }

    public List<Transportation> getTransportations() {
        return transportations;
    }

    public void setTransportations(List<Transportation> transportations) {
        this.transportations = transportations;
    }
}
