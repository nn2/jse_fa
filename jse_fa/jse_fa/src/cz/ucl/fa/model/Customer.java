package cz.ucl.fa.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Ondra on 23. 1. 2015.
 */
@Entity
@NamedQueries( {
        @NamedQuery(name = "Customer.withContracts", query = "SELECT DISTINCT cust from Customer cust join cust.contracts cont where cust.id = cont.customer"),
})
public class Customer {

    @Id
    @GeneratedValue
    private long id;
    private String surname;
    private String firstName;

    @ManyToOne
    private CreditCard card;

    @OneToMany(mappedBy = "customer")
    private List<Contract> contracts;

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public CreditCard getCard() {
        return card;
    }

    public void setCard(CreditCard card) {
        this.card = card;
    }

    public List<Contract> getContracts() {
        return contracts;
    }

    public void setContracts(List<Contract> contracts) {
        this.contracts = contracts;
    }

    public String getFullName() {
        return surname + " " + firstName;
    }
}
