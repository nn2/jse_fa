package cz.ucl.fa.model;

import cz.ucl.fa.model.util.JPAUtil;

import javax.persistence.*;
import java.text.DateFormat;
import java.util.Date;

/**
 * Created by Ondra on 23. 1. 2015.
 */
@Entity
public class Stay {

    @Id
    @GeneratedValue
    private long id;

    private Date dayFrom;
    private Date dayTo;

    @ManyToOne(cascade = CascadeType.ALL)
    private Holiday holiday;
    @ManyToOne(cascade = CascadeType.ALL)
    private Accommodation hotel;

    public Accommodation getHotel() {
        return hotel;
    }

    public void setHotel(Accommodation hotel) {
        this.hotel = hotel;
    }

    public Date getDayFrom() {
        return dayFrom;
    }

    public void setDayFrom(Date dayFrom) {
        this.dayFrom = dayFrom;
    }

    public Date getDayTo() {
        return dayTo;
    }

    public void setDayTo(Date dayTo) {
        this.dayTo = dayTo;
    }

    public Holiday getHoliday() {
        return holiday;
    }

    public void setHoliday(Holiday holiday) {
        this.holiday = holiday;
    }

    public boolean canAccommodate(int numberOfPeople) {
        javax.persistence.Query q = JPAUtil.getEntityManager().createNativeQuery("SELECT (capacity-count(stay.id)) FROM stay join holiday on holiday.id = stay.holiday_id join fadb.accommodation on accommodation.id = stay.hotel_id join contract on holiday.id = contract.holiday_id join traveller on traveller.contract_id = contract.id where stay.hotel_id = ?1 AND ((stay.dayFrom <= ?2 AND stay.dayTo >= ?2) OR (stay.dayFrom <= ?3 AND stay.dayTo >= ?3)) GROUP BY stay.id;");
        q.setParameter("1", getHotel());
        q.setParameter("2", getDayFrom());
        q.setParameter("3", getDayTo());
        try {
            return numberOfPeople <= Integer.parseInt(q.getSingleResult().toString());
        } catch (NoResultException ex) {
            // If no result, there are no reservations
            return numberOfPeople <= getHotel().getCapacity();
        }
    }

    @Override
    public String toString() {
        DateFormat df = DateFormat.getDateInstance();
        return String.format("%s, from %s to %s", hotel, df.format(dayFrom), df.format(dayTo));
    }
}
