package cz.ucl.fa.ui;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import cz.ucl.fa.model.*;
import cz.ucl.fa.model.util.JPAUtil;
import cz.ucl.fa.ui.steps.*;
import cz.ucl.fa.ui.steps.SellTripPanel;

import javax.persistence.EntityManager;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;

public class SellTripWizardDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonNext;
    private JButton buttonCancel;
    private JButton buttonPrevious;
    private JButton buttonFinish;
    private JPanel cards;
    private JLabel stepLabel;

    private static final int PREMADE_PATH = 1;
    private static final int CUSTOM_PATH = 2;

    private static final String STEP1 = "step1";
    private static final String STEP2A = "step2a";
    private static final String STEP2B = "step2b";
    private static final String STEP3 = "step3";
    private static final String STEP4 = "step4";
    private static final String STEP5 = "step5";

    private Map<Integer, List<String>> steps;
    private Integer currentPath;
    private int currentStep;

    private HashMap<String, SellTripPanel> panels;
    private SellTripPanel currentPanel;

    private Contract contract;

    public SellTripWizardDialog(Frame parent, boolean modal) {
        super(parent);
        setContentPane(contentPane);
        setModal(modal);

        //getRootPane().setDefaultButton(buttonNext);

        currentPath = PREMADE_PATH;

        contract = new Contract();

        initializeSteps();

        initializeStepsContent();

        buttonNext.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onNext();
            }
        });

        buttonPrevious.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onPrevious();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        buttonFinish.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onFinish();
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        enableButtons();
        updateTitle();

        pack();
        setLocationRelativeTo(getParent());
    }

    private void onNext() {
        if (currentStep == 0) {
            if (currentPanel.getResult() == TripType.CUSTOM) {
                currentPath = CUSTOM_PATH;
            } else {
                currentPath = PREMADE_PATH;
            }
        } else {
            if (!currentPanel.isStepValid()) {
                return;
            }
        }

        currentStep++;
        currentPanel = panels.get(steps.get(currentPath).get(currentStep));

        CardLayout cl = (CardLayout) cards.getLayout();
        updateTitle();
        enableButtons();
        currentPanel.updateComponents();
        cl.show(cards, steps.get(currentPath).get(currentStep));
    }

    private void onPrevious() {
        currentStep--;

        currentPanel = panels.get(steps.get(currentPath).get(currentStep));
        CardLayout cl = (CardLayout) cards.getLayout();
        updateTitle();
        enableButtons();
        cl.show(cards, steps.get(currentPath).get(currentStep));
    }

    private void onFinish() {
        EntityManager em = JPAUtil.getEntityManager();

        em.getTransaction().begin();
        for (Service s : contract.getHoliday().getServices()) {
            em.persist(s);
        }

        for (Transportation t : contract.getHoliday().getTransportations()) {
            em.persist(t);
        }

        for (Traveller t : contract.getTravellers()) {
            t.setContract(contract);
            em.persist(t);
        }

        for (Stay s : contract.getHoliday().getStays()) {
            em.persist(s);
        }

        em.persist(contract.getCustomer().getCard());
        em.persist(contract.getCustomer());
        em.persist(contract);
        em.flush();
        em.getTransaction().commit();

        contract.exportToFile();

        dispose();
    }

    /**
     * Initialize panels to be shown in wizard
     */
    private void initializeStepsContent() {
        panels = new HashMap<String, SellTripPanel>();
        panels.put(STEP1, new TripType(this, null));
        panels.put(STEP2A, new PredefinedTrip(this, contract));
        panels.put(STEP2B, new CustomTrip(this, contract));
        panels.put(STEP3, new TripReview(this, contract));
        panels.put(STEP4, new TripTravellers(this, contract));
        panels.put(STEP5, new TripFinalReview(this, contract));

        cards.add(panels.get(STEP1), STEP1);
        cards.add(panels.get(STEP2A), STEP2A);
        cards.add(panels.get(STEP2B), STEP2B);
        cards.add(panels.get(STEP3), STEP3);
        cards.add(panels.get(STEP4), STEP4);
        cards.add(panels.get(STEP5), STEP5);

        currentPanel = panels.get(steps.get(currentPath).get(currentStep));
    }

    /**
     * Initializes step names for paths
     */
    private void initializeSteps() {
        ArrayList<String> premadeSteps = new ArrayList<String>();
        premadeSteps.add(STEP1);
        premadeSteps.add(STEP2A);
        premadeSteps.add(STEP3);
        premadeSteps.add(STEP4);
        premadeSteps.add(STEP5);

        currentStep = 0;

        ArrayList<String> customSteps = (ArrayList<String>) premadeSteps.clone();
        customSteps.set(1, STEP2B);
        customSteps.remove(2);

        steps = new HashMap<Integer, List<String>>();
        steps.put(CUSTOM_PATH, customSteps);
        steps.put(PREMADE_PATH, premadeSteps);
    }

    private void onCancel() {
// add your code here if necessary
        dispose();
    }

    private void enableButtons() {
        buttonPrevious.setEnabled(currentStep > 0);
        buttonNext.setEnabled(canDoNext());
        buttonFinish.setEnabled(canDoFinish());
    }

    private void updateTitle() {
        setTitle(String.format("Sell trip - %d / %d", currentStep + 1, steps.get(currentPath).size()));
        stepLabel.setText(currentPanel.getStepName());
    }

    private boolean canDoFinish() {
        return currentStep == steps.get(currentPath).size() - 1;
    }

    private boolean canDoNext() {
        return currentStep < steps.get(currentPath).size() - 1;
    }

    @Override
    public void revalidate() {
        super.revalidate();
        enableButtons();
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        contentPane = new JPanel();
        contentPane.setLayout(new GridLayoutManager(3, 1, new Insets(10, 10, 10, 10), -1, -1));
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(1, 5, new Insets(0, 0, 0, 0), -1, -1));
        contentPane.add(panel1, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, 1, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        panel1.add(spacer1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        buttonNext = new JButton();
        buttonNext.setText("Next >>");
        panel1.add(buttonNext, new GridConstraints(0, 3, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        buttonPrevious = new JButton();
        buttonPrevious.setText("<< Previous");
        panel1.add(buttonPrevious, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        buttonFinish = new JButton();
        buttonFinish.setFont(new Font(buttonFinish.getFont().getName(), Font.BOLD, buttonFinish.getFont().getSize()));
        buttonFinish.setText("Confirm");
        panel1.add(buttonFinish, new GridConstraints(0, 4, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        buttonCancel = new JButton();
        buttonCancel.setText("Cancel");
        panel1.add(buttonCancel, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        cards = new JPanel();
        cards.setLayout(new CardLayout(0, 0));
        contentPane.add(cards, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        stepLabel = new JLabel();
        stepLabel.setText("Label");
        contentPane.add(stepLabel, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return contentPane;
    }
}
