package cz.ucl.fa.ui;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import cz.ucl.fa.model.*;
import cz.ucl.fa.ui.model.TravellersTableModel;
import org.hibernate.metamodel.ValidationException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

/**
 * Created by Ondra on 24. 1. 2015.
 */
public class SellTripAddTravellersDialog extends JDialog {

    private JTextField textCustomerFirstName;
    private JTextField textCustomerSurname;
    private JTextField textCustomerCreditCard;
    private JTextField textCustomerCardExpiry;
    private JTable tableTravellers;
    private JButton buttonAdd;
    private JButton buttonRemove;
    private JTextField textTravellerFirstName;
    private JTextField textTravellerSurname;
    private JTextField textTravellerDateOfBirth;
    private JTextField textTravellerIdNumber;
    private JPanel contentPane;

    private JDialog parentDialog = null;

    private Contract contract;
    private Contract parentContract;

    private TravellersTableModel tableTravellersModel;

    public SellTripAddTravellersDialog() {
        setContentPane(contentPane);

        initComponents();
    }

    public SellTripAddTravellersDialog(JDialog parentDialog) {
        setContentPane(contentPane);

        this.parentDialog = parentDialog;

        initComponents();
    }

    public SellTripAddTravellersDialog(JDialog parentDialog, Contract parentContract) {
        setContentPane(contentPane);

        this.parentDialog = parentDialog;
        this.parentContract = parentContract;

        initComponents();
    }

    private void initComponents() {
        tableTravellersModel = new TravellersTableModel();
        tableTravellers.setModel(tableTravellersModel);

        enableButtons();

        buttonAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onAdd();
            }
        });
        buttonRemove.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onRemove();
            }
        });
        tableTravellers.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                enableButtons();
            }
        });
    }

    private void enableButtons() {
        buttonRemove.setEnabled(tableTravellers.getSelectedRow() >= 0);
    }

    private void onAdd() {
        try {
            validateTraveller();
            Traveller traveller = new Traveller();

            traveller.setFirstName(textTravellerFirstName.getText());
            traveller.setSurname(textTravellerSurname.getText());
            traveller.setIdNumber(textTravellerIdNumber.getText());

            try {
                traveller.setDateOfBirth((DateFormat.getDateInstance()).parse(textTravellerDateOfBirth.getText()));
            } catch (ParseException ex) {

            }

            tableTravellersModel.add(traveller);

            textTravellerFirstName.setText("");
            textTravellerSurname.setText("");
            textTravellerDateOfBirth.setText("");
            textTravellerIdNumber.setText("");
        } catch (ValidationException ex) {
            JOptionPane.showMessageDialog(this.getParent(), ex.getMessage(), "Validation error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void onRemove() {
        tableTravellersModel.remove(tableTravellers.getSelectedRow());
    }

    private void validateTraveller() throws ValidationException {
        if (textTravellerFirstName.getText().isEmpty()) {
            throw new ValidationException("Traveller name can't be empty");
        }

        if (textTravellerSurname.getText().isEmpty()) {
            throw new ValidationException("Travellers surname can't be empty");
        }

        if (textTravellerDateOfBirth.getText().isEmpty()) {
            throw new ValidationException("Travellers date of birth can't be empty");
        }

        if (textTravellerIdNumber.getText().isEmpty()) {
            throw new ValidationException("Travellers ID numner can't be empty");
        }
        DateFormat df = DateFormat.getDateInstance();
        Date date;
        try {
            date = df.parse(textTravellerDateOfBirth.getText());
        } catch (ParseException ex) {
            throw new ValidationException("Please enter date in valid format");
        }
    }

    private void validateData() throws ValidationException {

        if (textCustomerFirstName.getText().isEmpty()) {
            throw new ValidationException("Customer name can't be empty");
        }

        if (textCustomerSurname.getText().isEmpty()) {
            throw new ValidationException("Customer surname can't be empty");
        }

        if (textCustomerCreditCard.getText().isEmpty()) {
            throw new ValidationException("Customer card number must be set");
        }

        if (!CreditCard.verifyNumber(textCustomerCreditCard.getText())) {
            throw new ValidationException("Please enter valid card number (Visa, MasterCard, JCB, Diners)");
        }

        if (textCustomerCardExpiry.getText().isEmpty()) {
            throw new ValidationException("Card expiration must be set");
        }

        int travellerCount = tableTravellersModel.getRowCount();

        if (tableTravellersModel.getRowCount() == 0) {
            throw new ValidationException("There must be at least one traveller");
        }

        // Validate vacancies in hotel
        for (Stay s : parentContract.getHoliday().getStays()) {
            if (!s.canAccommodate(travellerCount)) {
                throw new ValidationException(s.getHotel() + " can not accomodate " + travellerCount + " travellers");
            }
        }
    }

    public Contract validateData(boolean createContract) throws ValidationException {
        validateData();

        if (createContract) {
            createContract();
        }

        return contract;
    }

    private void createContract() {
        contract = new Contract();

        Customer customer = new Customer();
        CreditCard card = new CreditCard();


        customer.setFirstName(textCustomerFirstName.getText());
        customer.setSurname(textCustomerSurname.getText());

        card.setOwnerName(customer.getFullName());
        card.setNumber(textCustomerCreditCard.getText());
        card.setValidity(textCustomerCardExpiry.getText());
        customer.setCard(card);

        contract.setCustomer(customer);
        contract.setTravellers(tableTravellersModel.getAllTravellers());
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        contentPane = new JPanel();
        contentPane.setLayout(new GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1));
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(5, 2, new Insets(0, 0, 0, 0), -1, -1));
        contentPane.add(panel1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(700, -1), null, 0, false));
        final JLabel label1 = new JLabel();
        label1.setText("Customer");
        panel1.add(label1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        panel1.add(spacer1, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final JLabel label2 = new JLabel();
        label2.setText("First name");
        panel1.add(label2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label3 = new JLabel();
        label3.setText("Surname");
        panel1.add(label3, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label4 = new JLabel();
        label4.setText("Credit card");
        panel1.add(label4, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label5 = new JLabel();
        label5.setText("Card expiry");
        panel1.add(label5, new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        textCustomerFirstName = new JTextField();
        panel1.add(textCustomerFirstName, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        textCustomerSurname = new JTextField();
        panel1.add(textCustomerSurname, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        textCustomerCreditCard = new JTextField();
        panel1.add(textCustomerCreditCard, new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        textCustomerCardExpiry = new JTextField();
        panel1.add(textCustomerCardExpiry, new GridConstraints(4, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new GridLayoutManager(2, 2, new Insets(0, 0, 0, 0), -1, -1));
        contentPane.add(panel2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(-1, 200), null, 0, false));
        final JLabel label6 = new JLabel();
        label6.setText("Travellers");
        panel2.add(label6, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer2 = new Spacer();
        panel2.add(spacer2, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final JScrollPane scrollPane1 = new JScrollPane();
        panel2.add(scrollPane1, new GridConstraints(1, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        tableTravellers = new JTable();
        scrollPane1.setViewportView(tableTravellers);
        final JPanel panel3 = new JPanel();
        panel3.setLayout(new GridLayoutManager(4, 3, new Insets(0, 0, 0, 0), -1, -1));
        contentPane.add(panel3, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JPanel panel4 = new JPanel();
        panel4.setLayout(new GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel3.add(panel4, new GridConstraints(0, 2, 4, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        buttonAdd = new JButton();
        buttonAdd.setText("Add");
        panel4.add(buttonAdd, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        buttonRemove = new JButton();
        buttonRemove.setText("Remove selected");
        panel4.add(buttonRemove, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer3 = new Spacer();
        panel4.add(spacer3, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        final JLabel label7 = new JLabel();
        label7.setText("Surname");
        panel3.add(label7, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label8 = new JLabel();
        label8.setText("First name");
        panel3.add(label8, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label9 = new JLabel();
        label9.setText("Date of birth");
        panel3.add(label9, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label10 = new JLabel();
        label10.setText("ID num");
        panel3.add(label10, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        textTravellerFirstName = new JTextField();
        panel3.add(textTravellerFirstName, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        textTravellerSurname = new JTextField();
        panel3.add(textTravellerSurname, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        textTravellerDateOfBirth = new JTextField();
        panel3.add(textTravellerDateOfBirth, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        textTravellerIdNumber = new JTextField();
        panel3.add(textTravellerIdNumber, new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return contentPane;
    }
}
