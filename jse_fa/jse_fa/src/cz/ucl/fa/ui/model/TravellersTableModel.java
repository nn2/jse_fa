package cz.ucl.fa.ui.model;

import cz.ucl.fa.model.Traveller;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Ondra on 25. 1. 2015.
 */
public class TravellersTableModel extends AbstractTableModel {

    private static final int COLUMN_SURNAME = 0;
    private static final int COLUMN_NAME = 1;
    private static final int COLUMN_DATE_OF_BIRTH = 2;
    private static final int COLUMN_ID_NUMBER = 3;

    private List<Traveller> allTravellers;

    public TravellersTableModel() {
        allTravellers = new ArrayList<Traveller>();
        fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return allTravellers.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        Class<?> result = null;

        switch (columnIndex) {
            case COLUMN_NAME:
            case COLUMN_SURNAME:
            case COLUMN_ID_NUMBER:
                result = String.class;
                break;
            case COLUMN_DATE_OF_BIRTH:
                result = Date.class;
        }

        return result;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object result = null;

        if (rowIndex > allTravellers.size() - 1) {
            return result;
        }

        Traveller traveller = allTravellers.get(rowIndex);
        switch (columnIndex) {
            case COLUMN_NAME:
                result = traveller.getFirstName();
                break;
            case COLUMN_SURNAME:
                result = traveller.getSurname();
                break;
            case COLUMN_DATE_OF_BIRTH:
                result = traveller.getDateOfBirth();
                break;
            case COLUMN_ID_NUMBER:
                result = traveller.getIdNumber();
                break;
        }

        return result;
    }

    @Override
    public String getColumnName(int column) {
        String result = "";

        switch (column) {
            case COLUMN_NAME:
                result = "First name";
                break;
            case COLUMN_SURNAME:
                result = "Surname";
                break;
            case COLUMN_ID_NUMBER:
                result = "ID number";
                break;
            case COLUMN_DATE_OF_BIRTH:
                result = "Date of birth";
                break;
        }

        return result;
    }

    public void add(Traveller traveller) {
        allTravellers.add(traveller);
        fireTableDataChanged();
    }

    public void remove(int selectedRow) {
        allTravellers.remove(selectedRow);
        fireTableDataChanged();
    }

    public List<Traveller> getAllTravellers() {
        return allTravellers;
    }

    public void setAllTravellers(List<Traveller> travellers) {
        allTravellers.clear();
        allTravellers.addAll(travellers);
        fireTableDataChanged();
    }
}
