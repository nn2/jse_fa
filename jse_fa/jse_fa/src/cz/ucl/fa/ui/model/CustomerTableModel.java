package cz.ucl.fa.ui.model;

import cz.ucl.fa.model.Customer;
import cz.ucl.fa.model.util.JPAUtil;

import javax.persistence.EntityManager;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ondra on 23. 1. 2015.
 */
public class CustomerTableModel extends AbstractTableModel {


    private static final int COLUMN_SURNAME = 0;
    private static final int COLUMN_NAME = 1;
    private static final int COLUMN_CONTRACTS = 2;

    private List<Customer> allCustomers;

    private EntityManager em;

    public CustomerTableModel() {
        em = JPAUtil.getEntityManager();
        refreshCustomerList();
    }

    @Override
    public int getRowCount() {
        return allCustomers.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object result = null;

        switch (columnIndex) {
            case COLUMN_SURNAME:
                result = allCustomers.get(rowIndex).getSurname();
                break;
            case COLUMN_NAME:
                result = allCustomers.get(rowIndex).getFirstName();
                break;
            case COLUMN_CONTRACTS:
                result = allCustomers.get(rowIndex).getContracts().size();
                break;
        }

        return result;
    }
    @Override
    public Class<?> getColumnClass(int column) {
        Class<?> result = null;
        switch (column) {
            case COLUMN_SURNAME:
                result = String.class;
                break;
            case COLUMN_NAME:
                result = String.class;
                break;
            case COLUMN_CONTRACTS:
                result = Integer.class;
                break;
        }
        return result;
    }

    @Override
    public String getColumnName(int column) {
        String result = null;
        switch (column) {
            case COLUMN_SURNAME:
                result = "Surname";
                break;
            case COLUMN_NAME:
                result = "First Name";
                break;
            case COLUMN_CONTRACTS:
                result = "Contracts";
                break;
        }
        return result;
    }

    public Customer getCustomer(int firstIndex) {
        if (firstIndex>=0 && firstIndex<allCustomers.size()) return allCustomers.get(firstIndex);
        else return null;
    }

    public void refreshCustomerList() {
        allCustomers = new ArrayList<Customer>();
        em.clear();
        allCustomers = (List<Customer>) em.createNamedQuery("Customer.withContracts").getResultList();
        fireTableDataChanged();
    }

}
