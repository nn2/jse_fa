package cz.ucl.fa.ui;

import javax.swing.*;

/**
 * Created by Ondra on 25. 1. 2015.
 */
public class ViewHelpers {
    public static void expandAllTreeNodes(JTree tree) {
        for (int i = 0; i < tree.getRowCount(); i++) {
            tree.expandRow(i);
        }
    }
}
