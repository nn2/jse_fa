package cz.ucl.fa.ui;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import cz.ucl.fa.model.*;
import cz.ucl.fa.model.util.JPAUtil;
import cz.ucl.fa.ui.model.ListListModel;
import org.hibernate.metamodel.ValidationException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

public class EditHolidayDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonSave;
    private JButton buttonCancel;
    private JTextField holidayName;
    private JList listTransportation;
    private JButton buttonAddTransportation;
    private JButton buttonRemoveTransportation;
    private JList listAccommodation;
    private JButton buttonAddAccommodation;
    private JButton buttonRemoveAccommodation;
    private JList listServices;
    private JButton buttonAddServices;
    private JButton buttonRemoveServices;
    private JLabel labelPrice;
    private JTextField textPrice;

    private Holiday holiday;
    private final boolean isNew;
    private final boolean isFixed;

    private ListListModel listAccommodationModel;
    private ListListModel listTransportationModel;
    private ListListModel listServicesModel;

    public EditHolidayDialog(Dialog owner, Holiday holiday) {
        super(owner, true);
        JPAUtil.getEntityManager().refresh(holiday);
        this.holiday = holiday;
        isNew = false;
        isFixed = true;
        initComponents();
    }

    public EditHolidayDialog(Dialog owner) {
        super(owner, true);
        this.holiday = null;
        isNew = true;
        isFixed = true;
        initComponents();
    }

    public EditHolidayDialog(Dialog owner, boolean isFixed) {
        super(owner, true);
        this.holiday = null;
        isNew = true;
        this.isFixed = isFixed;
        initComponents();
    }

    private void initComponents() {
        setContentPane(contentPane);

        listAccommodationModel = new ListListModel();
        listTransportationModel = new ListListModel();
        listServicesModel = new ListListModel();

        listTransportation.setModel(listTransportationModel);
        listAccommodation.setModel(listAccommodationModel);
        listServices.setModel(listServicesModel);

        if (!isNew) {
            setTitle("Edit holiday " + holiday.getName());
            fillComponentsWithData();

            if (holiday.getContracts().size() > 0) {
                makeFormReadOnly(true);
            }
        } else {
            setTitle("Create new holiday");
        }

        buttonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onClose();
            }
        });

        buttonSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onSave();
            }
        });

        buttonAddTransportation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onAddTransportation();
            }
        });
        buttonRemoveTransportation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onRemoveTransportation();
            }
        });

        buttonAddAccommodation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onAddAccommodation();
            }
        });
        buttonRemoveAccommodation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onRemoveAccommodation();
            }
        });

        buttonAddServices.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onAddServices();
            }
        });
        buttonRemoveServices.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onRemoveServices();
            }
        });

        // On windows closing call onClose function
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onClose();
            }
        });

        // Register ESC to close window
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onClose();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        listTransportation.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    buttonRemoveTransportation.setEnabled(listTransportation.getSelectedIndex() >= 0);
                }

            }
        });

        listAccommodation.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    buttonRemoveAccommodation.setEnabled(listAccommodation.getSelectedIndex() >= 0);
                }

            }
        });

        listServices.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    buttonRemoveServices.setEnabled(listServices.getSelectedIndex() >= 0);
                }

            }
        });

        pack();
        setLocationRelativeTo(getParent());
    }

    public void removeButtons() {
        removeComponentRecursive(getContentPane(), buttonSave);
        removeComponentRecursive(getContentPane(), buttonCancel);
    }

    private void performSave() {
        if (isNew) {
            holiday = new Holiday();
        }

        holiday.setName(holidayName.getText());
        holiday.setFixed(isFixed);
        holiday.setPrice(Double.parseDouble(textPrice.getText()));

        holiday.setTransportations(listTransportationModel.getData());
        holiday.setStays(listAccommodationModel.getData());
        holiday.setServices(listServicesModel.getData());

        holiday.setStarts(((Transportation) listTransportationModel.getData().get(0)).getDeparture());
        holiday.setEnds(((Transportation) listTransportationModel.getData().get(listTransportationModel.getSize() - 1)).getArrival());


        JPAUtil.getEntityManager().getTransaction().begin();
        JPAUtil.getEntityManager().persist(holiday);
        JPAUtil.getEntityManager().flush();

        // Automatic cascade save not working
        for (Stay s : holiday.getStays()) {
            s.setHoliday(holiday);
            JPAUtil.getEntityManager().persist(s);
        }

        JPAUtil.getEntityManager().persist(holiday);
        JPAUtil.getEntityManager().flush();
        JPAUtil.getEntityManager().getTransaction().commit();

    }

    private void onSave() {
        try {
            validateData();
            performSave();
            onClose();
        } catch (ValidationException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "Validation failed", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Data validation for completeness
     */
    private void validateData() throws ValidationException {
        //Validate transport
        Date lastDate = null;
        String firstCity = null;
        String lastCity = null;

        if (holidayName.getText().isEmpty()) {
            throw new ValidationException("Holiday name can't be empty");
        }

        if (listTransportationModel.getSize() == 0) {
            throw new ValidationException("Some means of transport need to be entered");
        }

        int iter = listTransportationModel.getSize() - 1;
        for (Object t : listTransportationModel.getData()) {
            if (lastDate != null) {
                if (((Transportation) t).getDeparture().before(lastDate)) {
                    throw new ValidationException(t.toString() + " departs before previous flight arrival");
                }
            }
            if (firstCity == null) {
                firstCity = ((Transportation) t).getFrom();
            }

            if (iter > 0 && !hasAccommodation(((Transportation) t))) {
                throw new ValidationException("You have to have accommodation in each city you travel to");
            }

            lastCity = ((Transportation) t).getTo();
            lastDate = ((Transportation) t).getArrival();

            iter--;
        }

        if (!firstCity.equals(lastCity)) {
            throw new ValidationException("Trip must start and end in the same city");
        }

        if (textPrice.getText().isEmpty()) {
            throw new ValidationException("Price can't be empty");
        } else {
            try {
                Double price = Double.parseDouble(textPrice.getText());
                if (price < 1) {
                    throw new ValidationException("Price can't be zero or negative");
                }
            } catch (NumberFormatException ex) {
                throw new ValidationException("Price error: " + ex.getMessage());
            }
        }

        for (Object s : listServicesModel.getData()) {
            if (!hasAccommodation((Service) s)) {
                throw new ValidationException(s.toString() + " has no location or hotel in trip");
            }
        }

    }

    public Holiday validateData(boolean save) {
        validateData();
        if (save) {
            performSave();
        }
        return holiday;
    }


    /**
     * Return current form validity.
     *
     * @return
     */
    public boolean isDataValid() {
        try {
            validateData();
            return true;
        } catch (ValidationException ex) {
            return false;
        }
    }


    /**
     * Helper function to strip minutes from date, for day only comparation.
     *
     * @param date
     * @return
     * @throws ParseException
     */
    private Date stripMinutes(Date date) throws ParseException {
        DateFormat df = DateFormat.getDateInstance();
        return df.parse(df.format(date));
    }

    /**
     * Check if there are accommodations for given Transportation
     * - on a date of Transportaion check the location for a stay.
     * @param transportation
     * @return
     */
    private boolean hasAccommodation(Transportation transportation) {
        boolean result = false;
        try {
            for (Object s : listAccommodationModel.getData()) {
                if (((Stay) s).getDayFrom().compareTo(stripMinutes(transportation.getArrival())) < 1 && ((Stay) s).getDayTo().compareTo(stripMinutes(transportation.getArrival())) >= 0) {
                    result = ((Stay) s).getHotel().getLocation().equals(transportation.getFrom()) || ((Stay) s).getHotel().getLocation().equals(transportation.getTo());
                    break;
                }
            }
        } catch (ParseException ex) {
        }
        return result;
    }

    /**
     * Check if there is a accommodation in hotel/city for a service
     *
     * @param service
     * @return
     */
    private boolean hasAccommodation(Service service) {
        boolean result = false;

        for (Object s : listAccommodationModel.getData()) {
            if (service.getHotel() == null) {
                if (((Stay) s).getHotel().getLocation() == service.getLocation()) {
                    result = true;
                }
            } else {
                if (((Stay) s).getHotel() == service.getHotel()) {
                    result = true;
                }
            }
        }


        return result;
    }

    /**
     * Assigns the transportation from dialog to holiday object.
     */
    private void onAddTransportation() {
        SelectTransportationDialog transportDialog = new SelectTransportationDialog(this, true);
        transportDialog.setLocationRelativeTo(this);
        transportDialog.setVisible(true);

        if (!transportDialog.wasCancelled()) {
            listTransportationModel.add(transportDialog.getSelectedTransportation());
        }
    }

    /**
     * Remove selected transportation
     */
    private void onRemoveTransportation() {
        if (JOptionPane.showConfirmDialog(this, "Really delete?",
                "Confirmation", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            int[] indices = listTransportation.getSelectedIndices();
            for (int i = indices.length - 1; i > -1; i--) {
                listTransportationModel.remove(indices[i]);
            }
        }

    }

    /**
     * Assigns the accommodation to holiday object.
     */
    private void onAddAccommodation() {
        SelectAccommodationDialog accomodationDialog = new SelectAccommodationDialog(this, true);
        accomodationDialog.setLocationRelativeTo(this);
        accomodationDialog.setVisible(true);

        if (!accomodationDialog.wasCancelled()) {
            Stay stay = accomodationDialog.getSelectedAccommodation();
            if (stay != null) {
                listAccommodationModel.add(stay);
            }
        }
    }

    /**
     * Remove selected accommodation
     */
    private void onRemoveAccommodation() {
        if (JOptionPane.showConfirmDialog(this, "Really delete?",
                "Confirmation", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            int[] indices = listAccommodation.getSelectedIndices();
            for (int i = indices.length - 1; i > -1; i--) {
                listAccommodationModel.remove(indices[i]);
            }
        }
    }

    /**
     * Assigns the service to holiday object.
     */
    private void onAddServices() {
        SelectServiceDialog serviceDialog = new SelectServiceDialog(this, true);
        serviceDialog.setLocationRelativeTo(this);
        serviceDialog.setVisible(true);

        if (!serviceDialog.wasCancelled()) {
            listServicesModel.add(serviceDialog.getSelectedService());
        }
    }

    /**
     * Remove selected service
     */
    private void onRemoveServices() {
        if (JOptionPane.showConfirmDialog(this, "Really delete?",
                "Confirmation", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            int[] indices = listServices.getSelectedIndices();
            for (int i = indices.length; i > -1; i--) {
                listServicesModel.remove(indices[i]);
            }
        }

    }

    /**
     * Load dialog components with appropriate data.
     */
    private void fillComponentsWithData() {
        holidayName.setText(holiday.getName());

        textPrice.setText(holiday.getPrice().toString());

        listTransportationModel.setData(holiday.getTransportations());
        listAccommodationModel.setData(holiday.getStays());
        listServicesModel.setData(holiday.getServices());
    }

    /**
     * Helper function to cycle all component from a container and dis/en-able.
     *
     * @param container
     * @param enabled
     */
    private void enableComponents(Container container, boolean enabled) {
        for (Component c : container.getComponents()) {
            c.setEnabled(enabled);
            if (c instanceof Container) {
                enableComponents((Container) c, enabled);
            }
        }
    }

    private void removeComponentRecursive(Container container, Component component) {
        container.remove(component);
        for (Component c : container.getComponents()) {
            if (c instanceof Container) {
                removeComponentRecursive((Container) c, component);
            }
        }
    }

    /**
     * Make all components in the dialog read only (or not)
     *
     * @param readOnly
     */
    private void makeFormReadOnly(boolean readOnly) {
        enableComponents(getContentPane(), !readOnly);

        // Want to be able at least close the dialog
        buttonCancel.setEnabled(true);
    }

    private void onClose() {
        dispose();
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        contentPane = new JPanel();
        contentPane.setLayout(new GridLayoutManager(2, 1, new Insets(10, 10, 10, 10), -1, -1));
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(1, 4, new Insets(0, 0, 0, 0), -1, -1));
        contentPane.add(panel1, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, 1, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        panel1.add(spacer1, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1, true, false));
        panel1.add(panel2, new GridConstraints(0, 3, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        buttonSave = new JButton();
        buttonSave.setText("Save");
        panel2.add(buttonSave, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        buttonCancel = new JButton();
        buttonCancel.setText("Cancel");
        panel2.add(buttonCancel, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        labelPrice = new JLabel();
        labelPrice.setText("Price:");
        panel1.add(labelPrice, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        textPrice = new JTextField();
        panel1.add(textPrice, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(50, -1), null, 1, false));
        final JPanel panel3 = new JPanel();
        panel3.setLayout(new GridLayoutManager(4, 1, new Insets(0, 0, 0, 0), -1, -1));
        contentPane.add(panel3, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(700, 400), null, 0, false));
        final JPanel panel4 = new JPanel();
        panel4.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        panel3.add(panel4, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, new Dimension(-1, 10), null, 0, false));
        final JLabel label1 = new JLabel();
        label1.setText("Name:");
        panel4.add(label1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 1, false));
        holidayName = new JTextField();
        holidayName.setText("");
        panel4.add(holidayName, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, 10), null, 0, false));
        final JPanel panel5 = new JPanel();
        panel5.setLayout(new GridLayoutManager(2, 2, new Insets(0, 0, 0, 0), -1, -1));
        panel3.add(panel5, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JPanel panel6 = new JPanel();
        panel6.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel5.add(panel6, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, new Dimension(160, -1), 0, false));
        buttonAddTransportation = new JButton();
        buttonAddTransportation.setText("Add");
        panel6.add(buttonAddTransportation, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, new Dimension(150, -1), 0, false));
        buttonRemoveTransportation = new JButton();
        buttonRemoveTransportation.setEnabled(false);
        buttonRemoveTransportation.setText("Remove selected");
        panel6.add(buttonRemoveTransportation, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, new Dimension(150, -1), 0, false));
        final JLabel label2 = new JLabel();
        label2.setText("Transportation:");
        panel5.add(label2, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, new Dimension(105, -1), null, null, 0, false));
        listTransportation = new JList();
        panel5.add(listTransportation, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(500, -1), null, 0, false));
        final JPanel panel7 = new JPanel();
        panel7.setLayout(new GridLayoutManager(2, 2, new Insets(0, 0, 0, 0), -1, -1));
        panel3.add(panel7, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JPanel panel8 = new JPanel();
        panel8.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel7.add(panel8, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, new Dimension(160, -1), 0, false));
        buttonAddAccommodation = new JButton();
        buttonAddAccommodation.setText("Add");
        panel8.add(buttonAddAccommodation, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, new Dimension(150, -1), 0, false));
        buttonRemoveAccommodation = new JButton();
        buttonRemoveAccommodation.setEnabled(false);
        buttonRemoveAccommodation.setText("Remove selected");
        panel8.add(buttonRemoveAccommodation, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, new Dimension(150, -1), 0, false));
        final JLabel label3 = new JLabel();
        label3.setText("Accommodation:");
        panel7.add(label3, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, new Dimension(105, -1), null, null, 0, false));
        listAccommodation = new JList();
        panel7.add(listAccommodation, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(500, -1), null, 0, false));
        final JPanel panel9 = new JPanel();
        panel9.setLayout(new GridLayoutManager(2, 2, new Insets(0, 0, 0, 0), -1, -1));
        panel3.add(panel9, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JPanel panel10 = new JPanel();
        panel10.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel9.add(panel10, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, new Dimension(160, -1), 0, false));
        buttonAddServices = new JButton();
        buttonAddServices.setText("Add");
        panel10.add(buttonAddServices, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, new Dimension(150, -1), 0, false));
        buttonRemoveServices = new JButton();
        buttonRemoveServices.setEnabled(false);
        buttonRemoveServices.setText("Remove selected");
        panel10.add(buttonRemoveServices, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, new Dimension(150, -1), 0, false));
        final JLabel label4 = new JLabel();
        label4.setText("Other services:");
        panel9.add(label4, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, new Dimension(105, -1), null, null, 0, false));
        listServices = new JList();
        panel9.add(listServices, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(500, -1), null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return contentPane;
    }
}
