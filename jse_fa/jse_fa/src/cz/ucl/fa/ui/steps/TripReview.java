package cz.ucl.fa.ui.steps;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import cz.ucl.fa.model.Contract;
import cz.ucl.fa.ui.ViewHelpers;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.awt.*;

/**
 * Created by Ondra on 24. 1. 2015.
 */
public class TripReview extends SellTripPanel {

    private JTree tree1;
    private DefaultMutableTreeNode root;
    private JLabel label1;

    public TripReview(JDialog parent, Object result) {
        super(parent, result);
        
        initComponents();
    }

    private void initComponents() {
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        label1 = new JLabel();
        label1.setText("Price: ");

        panel1.add(label1, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        tree1 = new JTree();
        root = new DefaultMutableTreeNode("Review");

        if (((Contract) result).getHoliday() != null) {
            Contract.buildContractTree(root, (Contract) result);
        }

        tree1.setModel(new DefaultTreeModel(root));

        panel1.add(tree1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_NORTHWEST, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(600,400), null, 0, false));

        this.add(panel1);
    }

    @Override
    public boolean isStepValid() {
        return true;
    }

    @Override
    public void revalidate() {
        super.revalidate();

        if (result != null && ((Contract) result).getHoliday() != null) {
            root.removeAllChildren();
            Contract.buildContractTree(root, (Contract) result);
            tree1.setModel(new DefaultTreeModel(root));

            label1.setText("Price: " + ((Contract) result).getHoliday().getPrice());

            ViewHelpers.expandAllTreeNodes(tree1);

        }

    }

    @Override
    public Object getResult() {
        return this.result;
    }

    @Override
    public String getStepName() {
        return "Trip overview:";
    }
}
