package cz.ucl.fa.ui.steps;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Ondra on 24. 1. 2015.
 */
public class TripType extends SellTripPanel {
    private JRadioButton radioButton1;
    private JRadioButton radioButton2;
    private ButtonGroup bgTripType;

    public static final String CUSTOM = "Custom";
    public static final String PREDEFINED = "Pre-defined";

    private Object parent;

    public TripType(JDialog parent, Object result) {
        super(parent, result);

        this.parent = parent;

        initComponents();

    }

    @Override
    public boolean isStepValid() {
        // Any choice is valid;
        return true;
    }

    @Override
    public Object getResult() {
        return radioButton1.isSelected() ? radioButton1.getText() : radioButton2.getText();
    }

    @Override
    public String getStepName() {
        return "Select trip type:";
    }

    private void initComponents() {
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        radioButton1 = new JRadioButton();
        radioButton1.setText(PREDEFINED);
        radioButton1.setSelected(true);
        panel1.add(radioButton1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        radioButton2 = new JRadioButton();
        radioButton2.setText(CUSTOM);
        panel1.add(radioButton2, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));

        bgTripType = new javax.swing.ButtonGroup();
        bgTripType.add(radioButton1);
        bgTripType.add(radioButton2);

        this.add(panel1);
    }
}
