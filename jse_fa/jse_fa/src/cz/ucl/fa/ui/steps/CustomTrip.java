package cz.ucl.fa.ui.steps;

import cz.ucl.fa.model.Contract;
import cz.ucl.fa.ui.EditHolidayDialog;
import org.hibernate.metamodel.ValidationException;

import javax.swing.*;

/**
 * Created by Ondra on 25. 1. 2015.
 */
public class CustomTrip extends SellTripPanel {

    EditHolidayDialog editHolidayDialog;

    public CustomTrip(JDialog parent, Object result) {
        super(parent, result);

        initComponents();
    }

    private void initComponents() {

        editHolidayDialog = new EditHolidayDialog(null, false);
        editHolidayDialog.removeButtons();
        this.add(editHolidayDialog.getContentPane());

    }

    @Override
    public boolean isStepValid() {
        try {
            ((Contract) result).setHoliday(editHolidayDialog.validateData(true));
            return true;
        } catch (ValidationException ex) {
            JOptionPane.showMessageDialog(getParent(), ex.getMessage(), "Validation error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

    @Override
    public Object getResult() {
        return null;
    }

    @Override
    public String getStepName() {
        return "Create custom trip:";
    }
}
