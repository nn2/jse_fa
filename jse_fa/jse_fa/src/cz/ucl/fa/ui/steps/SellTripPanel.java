package cz.ucl.fa.ui.steps;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Ondra on 24. 1. 2015.
 */
public abstract class SellTripPanel extends JPanel {

    protected Object result;
    protected JDialog parentDialog;


    public SellTripPanel(JDialog parent, Object result) {
        this.result = result;
        parentDialog = parent;
    }

    protected void notifyParent() {
        parentDialog.revalidate();
    }

    public void setResult(Object result) {
        this.result = result;
    }

    abstract public boolean isStepValid();

    abstract public Object getResult();

    abstract public String getStepName();

    public void updateComponents() {}
}
