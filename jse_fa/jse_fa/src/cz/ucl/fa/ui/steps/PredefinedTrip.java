package cz.ucl.fa.ui.steps;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import cz.ucl.fa.model.Contract;
import cz.ucl.fa.model.Holiday;
import cz.ucl.fa.ui.model.TripTableModel;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;

/**
 * Created by Ondra on 24. 1. 2015.
 */
public class PredefinedTrip extends SellTripPanel {
    private JPanel panel1;
    private JTable table1;
    private TripTableModel tripTableModel;
    private Holiday selectedHoliday;

    public PredefinedTrip(JDialog parent, Contract contract) {
        super(parent, contract);

        parentDialog = parent;

        initComponents();
    }

    private void initComponents() {
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        final JScrollPane scrollPane1 = new JScrollPane();
        panel1.add(scrollPane1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, true));

        tripTableModel = new TripTableModel();
        table1 = new JTable();
        table1.setModel(tripTableModel);

        table1.getSelectionModel().addListSelectionListener(
                new ListSelectionListener() {
                    public void valueChanged(ListSelectionEvent e) {
                        selectedHoliday = null;

                        if (table1.getSelectedRow() >= 0) {
                            selectedHoliday = tripTableModel.getTrip(((DefaultListSelectionModel) e.getSource()).getAnchorSelectionIndex());
                        }

                        notifyParent();
                    }
                }
        );

        table1.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);


        scrollPane1.setViewportView(table1);

        this.add(panel1);
    }

    @Override
    public boolean isStepValid() {
        if (result == null) {
            return false;
        }

        if (selectedHoliday != null) {
            ((Contract) result).setHoliday(selectedHoliday);
            return true;
        }

        return false;
    }

    @Override
    public Object getResult() {
        return null;
    }

    @Override
    public String getStepName() {
        return "Select pre-defined trip";
    }
}
