package cz.ucl.fa.ui.steps;

import cz.ucl.fa.model.Contract;
import cz.ucl.fa.ui.SellTripFinalDialog;

import javax.swing.*;

/**
 * Created by Ondra on 25. 1. 2015.
 */
public class TripFinalReview extends SellTripPanel {
    SellTripFinalDialog tripFinalDialog;


    public TripFinalReview(JDialog parent, Contract contract) {
        super(parent, contract);

        initComponents();
    }

    private void initComponents() {
        tripFinalDialog = new SellTripFinalDialog((Contract) result);

        this.add(tripFinalDialog.getContentPane());
    }

    public void updateComponents() {
        tripFinalDialog.updateComponents();
    }

    @Override
    public boolean isStepValid() {
        return false;
    }

    @Override
    public Object getResult() {
        return null;
    }

    @Override
    public String getStepName() {
        return "Customer and travellers";
    }
}
