package cz.ucl.fa.ui.steps;

import cz.ucl.fa.model.Contract;
import cz.ucl.fa.ui.SellTripAddTravellersDialog;
import org.hibernate.metamodel.ValidationException;

import javax.swing.*;

/**
 * Created by Ondra on 25. 1. 2015.
 */
public class TripTravellers extends SellTripPanel {

    SellTripAddTravellersDialog sellTripAddTravellersDialog;


    public TripTravellers(JDialog parent, Contract contract) {
        super(parent, contract);

        initComponents();
    }

    private void initComponents() {
        sellTripAddTravellersDialog = new SellTripAddTravellersDialog(parentDialog, (Contract) result);

        this.add(sellTripAddTravellersDialog.getContentPane());
    }

    @Override
    public boolean isStepValid() {
        try {
            Contract temporaryContract = sellTripAddTravellersDialog.validateData(true);

            if (temporaryContract == null) {
                return false;
            } else {
                ((Contract) result).setCustomer(temporaryContract.getCustomer());
                ((Contract) result).setTravellers(temporaryContract.getTravellers());
                ((Contract) result).setNumber((int) (Math.random()*100000));
            }
            return true;
        } catch (ValidationException ex) {
            JOptionPane.showMessageDialog(getParent(), ex.getMessage(), "Validation error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }

    @Override
    public Object getResult() {
        return null;
    }

    @Override
    public String getStepName() {
        return "Customer and travellers";
    }
}
